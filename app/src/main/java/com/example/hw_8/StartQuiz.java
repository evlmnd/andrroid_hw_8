package com.example.hw_8;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class StartQuiz extends AppCompatActivity {

    private final int maxLives = 3;

    private int questionCount = 0;
    private int lives = 3;

    ArrayList<Question> questionSet = new ArrayList<>();

    {
        questionSet.add(new Question("Is this a question?", true));
        questionSet.add(new Question("Is this IOS app?", false));
        questionSet.add(new Question("Is this Android app?", true));
        questionSet.add(new Question("Is it winter outside now?", false));
        questionSet.add(new Question("2020 sucks?", true));
        questionSet.add(new Question("2 + 2 = 4 ?", true));
        questionSet.add(new Question("Are you checking Galya's homework?", true));
        questionSet.add(new Question("Are you a robot?", false));
        questionSet.add(new Question("Are you a sure?", true));
        questionSet.add(new Question("Is watermelon a berry?", true));
        questionSet.add(new Question("Is MacOS best OS on Earth?", true));
    }

    @SuppressLint("SetTextI18n")
    private void renderQuestionCount() {
        TextView progressText = findViewById(R.id.progress_text);
        progressText.setText(questionCount + 1 + "/10");
    }

    private void renderQuestionText() {
        TextView questionText = findViewById(R.id.question_text);
        String text = questionSet.get(questionCount).getQuestionText();
        questionText.setText(text);
    }

    private Boolean isAnswerCorrect(Question question, Boolean userAnswerValue) {
        Boolean questionAnswer = question.getQuestionAnswer();
        return (userAnswerValue == questionAnswer);
    }

    @SuppressLint("SetTextI18n")
    private void renderLives() {

        if (lives == 0) {

            Toast toast = Toast.makeText(this, "You lose!", Toast.LENGTH_LONG);
            toast.getView().setBackgroundColor(Color.parseColor("#F6AE2D"));
            toast.show();

            SystemClock.sleep(500);

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if (lives < maxLives) {
            mistakeAlert();
        }

        TextView livesText = findViewById(R.id.lives_text);
        livesText.setText("Lives left: " + lives + "/" + maxLives);

//        пыталась нарендерить картинки
//        LinearLayout livesBlock = (LinearLayout)findViewById(R.id.lives_block);
//        for (int i = 0; i < lives; ++i) {
//            ImageView imageView = new ImageView(this);
//
//            imageView.setImageDrawable(getDrawable(R.drawable.life));
//            LinearLayout.LayoutParams params = new LinearLayout
//                    .LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
//            imageView.setLayoutParams(params);
//
//            livesBlock.addView(imageView);
//        }
    }

    private void switchLives() {
        lives -= 1;
    }

    public void mistakeAlert(){
        Toast toast = Toast.makeText(this, "Mistake! ц ц ц", Toast.LENGTH_LONG);
        toast.getView().setBackgroundColor(Color.parseColor("#F6AE2D"));
        toast.show();
    }

    public void correctAlert(){
        Toast toast = Toast.makeText(this, "Right!", Toast.LENGTH_LONG);
        toast.getView().setBackgroundColor(Color.parseColor("#FF85E648"));
        toast.show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_quiz);
        renderLives();
        renderQuestionText();
        renderQuestionCount();
    }


    public void clickTrue(View view) {
        if (!isAnswerCorrect(questionSet.get(questionCount), true)) {
            switchLives();
            renderLives();
        } else {
            correctAlert();
        }
        questionCount++;
        renderQuestionCount();
        renderQuestionText();

    }

    public void clickFalse(View view) {
        if (!isAnswerCorrect(questionSet.get(questionCount), false)) {
            switchLives();
            renderLives();
        } else {
            correctAlert();
        }
        questionCount++;
        renderQuestionCount();
        renderQuestionText();

    }
}

