package com.example.hw_8;

import java.util.ArrayList;
import java.util.List;

public class Question {
    private String questionText;
    private Boolean questionAnswer;

    Question(String text, Boolean answer) {
        this.questionText = text;
        this.questionAnswer = answer;
    }

    public String getQuestionText() {
        return questionText;
    }

    public Boolean getQuestionAnswer() {
        return questionAnswer;
    }

    @Override
    public String toString() {
        return getQuestionText();
    }



}


